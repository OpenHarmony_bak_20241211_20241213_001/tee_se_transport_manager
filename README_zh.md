# tee_se_transport_manager仓介绍 #

tee_se_transport_manager部件主要包含通道管理以及SCP协议，下面是具体的模块介绍。

### 一、tee_se_transport_manager部件模块划分 ###
<table>
<th>子模块名称</th>
<th>模块职责</th>
<tr>
<td> 通道管理 </td><td>负责创建跟芯片交互的通道逻辑</td>
</tr><tr>
<td> scp协议</td><td>负责创建SCP03协议</td>
</tr>
</table>

### 二、tee_se_transport_manager部件代码目录结构 ###
```
base/tee/tee_se_transport_manager
├── build
│   ├── lite
│   │   ├── build_client
│   │   └── build_service
│   └── standard
│       ├── build_client
│       └── build_service
├── bundle.json
├── LICENSE
├── README.md
├── README_zh.md
├── secure_channel
├── se_driver_hal
│   ├── nontee
│   │   └── se_drv_hal
│   └── tee
│       └── se_drv_hal
├── se_service
│   ├── include
│   └── src
│       ├── se_ipc
│       └── se_service_main
├── se_transport
│   ├── include
│   └── src
│       ├── se_api
│       └── se_ipc_client
└── test
    └── unittest

```