/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef INSE_EXT_H_
#define INSE_EXT_H_

int scard_connect(int reader_id, unsigned int vote_id, void *p_atr, unsigned int *atr_len);
int scard_disconnect(int reader_id, unsigned int vote_id);
int scard_transmit(int reader_id, unsigned char *p_cmd, unsigned int cmd_len, unsigned char *p_rsp,
                   unsigned int *rsp_len);
int scard_support_mode(int reader_id);
int scard_send(int reader_id, unsigned char *p_cmd, unsigned int cmd_len);
int scard_receive(unsigned char *p_rsp, unsigned int *rsp_len);
int scard_get_status(void);
int scard_get_ese_type(void);

#endif
